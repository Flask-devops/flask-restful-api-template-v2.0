from flask_sqlalchemy import SQLAlchemy
from flask_jwt_extended import JWTManager
from flask_apscheduler import APScheduler
from apscheduler.schedulers.background import BackgroundScheduler

# db
db = SQLAlchemy()

# jwt
jwt = JWTManager()

# scheduler计划任务
scheduler = APScheduler(BackgroundScheduler(timezone="Asia/Shanghai"))
