from . import logs_bp
import logging

logger = logging.getLogger(__name__)


@logs_bp.route('/testlogs', methods=["GET"])
def logs_test():
    logger.info("this is info")
    logger.debug("this is debug")
    logger.warning("this is warning")
    logger.error("this is error")
    logger.critical("this is critical")
    return "ok"
