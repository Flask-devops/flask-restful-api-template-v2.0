from app.utils.redis_util import Redis
from . import redis_bp


@redis_bp.route('/testRedisWrite', methods=['GET'])
def test_redis_write():
    """
    测试redis
    """
    Redis.write("test_key", "test_value", 60)
    return "ok"


@redis_bp.route('/testRedisRead', methods=['GET'])
def test_redis_read():
    """
    测试redis
    """
    data = Redis.read("test_key")
    return data
