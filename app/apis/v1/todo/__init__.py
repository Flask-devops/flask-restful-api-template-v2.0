from flask import Blueprint

# 定义蓝图对象todo_v1_bp
# 第一个参数：蓝图的名称
# 第二个参数：该蓝图所在的模块名称
# 第三个参数：指定页面的URL前缀
todo_v1_bp = Blueprint('todo_v1', __name__, url_prefix="/api/v1.0")
