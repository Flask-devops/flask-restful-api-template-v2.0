from datetime import datetime
from decimal import Decimal
from . import response_msg_bp
from flask import jsonify
from app.utils.code_msg import ResponseCode
from app.utils.response import ResMsg
from app.utils.util import route


@response_msg_bp.route("/test_msg", methods=["GET"])
def test_msg():
    """
    测试响应
    :return:
    """
    res = ResMsg()
    test_dict = dict(name="zhang", age=18)
    # 此处只需要填入响应状态码,即可获取到对应的响应消息
    res.update(code=ResponseCode.Success, data=test_dict)
    return jsonify(res.data)


@route(response_msg_bp, "/packed_response", methods=["GET"])
def test_packed_response():
    """
    测试响应封装
    :return:
    """
    res = ResMsg()
    test_dict = dict(name="zhang", age=18)
    # 此处只需要填入响应状态码,即可获取到对应的响应消息
    res.update(code=ResponseCode.Success, data=test_dict)
    # 此处不再需要用jsonify，如果需要定制返回头或者http响应如下所示
    # return res.data,200,{"token":"111"}
    return res.data


@route(response_msg_bp, '/type_response', methods=["GET"])
def test_type_response():
    """
    测试响应封装返回不同的类型
    :return:
    """
    res = ResMsg()

    now = datetime.now()
    date = datetime.now().date()
    num = Decimal(11.11)
    test_dict = dict(now=now, date=date, num=num)
    # 此处只需要填入响应状态码,即可获取到对应的响应消息
    res.update(code=ResponseCode.Success, data=test_dict)
    # 此处不再需要用jsonify，如果需要定制返回头或者http响应如下所示
    # return res.data,200,{"token":"111"}
    return res.data
