from .celery import add, flask_app_context
from . import celery_bp


@celery_bp.route('/testAdd', methods=["GET"])
def task_add():
    """
    测试相加
    :return:
    """
    result = add.delay(1, 2)
    return result.get(timeout=1)


@celery_bp.route('/testFlaskAppContext', methods=["GET"])
def task_flask_app_context():
    """
    测试获取flask上下文
    :return:
    """
    result = flask_app_context.delay()
    return result.get(timeout=1).replace('<Config', '')
