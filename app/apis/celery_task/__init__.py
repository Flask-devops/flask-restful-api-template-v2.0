from flask import Blueprint

celery_bp = Blueprint("celery_task", __name__, url_prefix="/api")
