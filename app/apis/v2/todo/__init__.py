from flask import Blueprint
from .views import TaskAPI, TaskListAPI
from flask_restful import Api


# 定义蓝图对象todo_v1_bp
# 第一个参数：蓝图的名称
# 第二个参数：该蓝图所在的模块名称
# 第三个参数：指定页面的URL前缀
todo_v2_bp = Blueprint('todo_v2', __name__, url_prefix="/api/v2.0")
api = Api(todo_v2_bp, default_mediatype="application/json")

api.add_resource(TaskListAPI, '/tasks', endpoint='tasks')
api.add_resource(TaskAPI, '/tasks/<int:id>', endpoint='task')
