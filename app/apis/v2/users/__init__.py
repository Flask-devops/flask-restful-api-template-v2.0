from flask import Blueprint
from .views import Register, Login, UserList, Userinfo, TokenRefresh, UserLogoutAccess, UserLogoutRefresh
from flask_restful import Api

# 定义蓝图对象user_v1_bp
# 第一个参数：蓝图的名称
# 第二个参数：该蓝图所在的模块名称
# 第三个参数：指定页面的URL前缀
user_v2_bp = Blueprint('user_v2', __name__, url_prefix="/api/v2.0")
api = Api(user_v2_bp, default_mediatype="application/json")

api.add_resource(Register, '/register', endpoint='register')
api.add_resource(Login, '/login', endpoint='login')
api.add_resource(UserList, '/users', endpoint='users')
api.add_resource(Userinfo, '/user/<int:id>', endpoint='user')
api.add_resource(TokenRefresh, '/token/refresh')
api.add_resource(UserLogoutAccess, '/logout/access')
api.add_resource(UserLogoutRefresh, '/logout/refresh')
