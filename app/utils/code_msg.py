"""
服务器常见的状态码
100：初始请求已接收，客户应该继续发送请求的其余部分
200：请求成功，
202：已接收请求，但未处理完成
204：服务器成功处理了请求，但未返回什么内容
302：临时性重定向
400：请求的语法错误，服务器无法解析
403：拒绝执行此请求
404：请求失败，所请求的资源在服务器上找不到
500：服务器内部错误，无法完成请求
"""


class ResponseCode(object):
    Success = 200  # 成功
    Fail = 404  # 失败
    RequestToRepeat = 401  # 资源重复
    NoResourceFound = 40001  # 未找到资源
    InvalidParameter = 40002  # 参数无效
    AccountOrPassWordErr = 40003  # 账户或密码错误
    VerificationCodeError = 40004  # 验证码错误
    PleaseSignIn = 40005  # 请登陆
    WeChatAuthorizationFailure = 40006  # 微信授权失败
    InvalidOrExpired = 40007  # 验证码过期
    MobileNumberError = 40008  # 手机号错误
    FrequentOperation = 40009  # 操作频繁,请稍后再试


class ResponseMessage(object):
    Success = "成功"
    Fail = "失败"
    RequestToRepeat = "资源重复"
    NoResourceFound = "未找到资源"
    InvalidParameter = "参数无效"
    AccountOrPassWordErr = "账户或密码错误"
    VerificationCodeError = "验证码错误"
    PleaseSignIn = "请登陆"
