"""
参考文献：
https://xugaoxiang.com/2020/10/08/flask-20-apscheduler/
https://www.qinzhiqiang.xyz/article/9


Flask结合APScheduler实现定时任务框架踩坑点
https://www.cnblogs.com/zncleon/p/14299706.html
"""
from datetime import datetime
from ext import scheduler, db
from app.models.users import User


# interval examples
def task1():
    print(datetime.now().strftime('%Y-%m-%d %H:%M:%S') + " task1 任务执行了")


def task2(a, b):
    print(str(a) + ' ' + str(b) + " task2 任务执行了")


def task3():
    print(datetime.now().strftime('%Y-%m-%d %H:%M:%S') + " task3 任务执行了")


# cron examples
# 每60s执行一次
@scheduler.task('interval', id='do_job_1', seconds=30, misfire_grace_time=900)
def do_job_1():
    print(datetime.now().strftime('%Y-%m-%d %H:%M:%S') + ' do_job_1 任务执行了')
    # with open('job.txt', 'a+') as f:
    #     f.write(str(datetime.now()) + "\n")


# 每分钟执行一次
@scheduler.task('cron', id='do_job_2', minute='*')
def do_job_2():
    print(datetime.now().strftime('%Y-%m-%d %H:%M:%S') + ' do_job_2 任务执行了')


# 每周执行一次
@scheduler.task('cron', id='do_job_3', week='*', day_of_week='sun')
def do_job_3():
    print(datetime.now().strftime('%Y-%m-%d %H:%M:%S') + ' do_job_3 任务执行了')


# 每天的13:26:05时刻执行一次函数
@scheduler.task('cron', id='do_job_3', day='*', hour='13', minute='26', second='05')
def do_job_4():
    print(str(datetime.now()) + ' do_job_4 任务执行了')


# task.py中使用数据库
def db_query_todo_all():
    """
    定时任务使用数据库
    """
    # 一定是要导入scheduler，这个是带有app及其上下文的，如果直接用current_app替代scheduler.app使用上下文
    with scheduler.app.app_context():
        data = db.session.query(User).first()
        print(str(datetime.now()) + " " + data.username)
