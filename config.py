import os
from apscheduler.jobstores.sqlalchemy import SQLAlchemyJobStore

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    """配置参数"""
    PORT = 5000
    HOST = "0.0.0.0"

    # 设置秘钥，在使用 CSRF 时必须
    SECRET_KEY = "my flask app"

    # ------------------------------  MYSQL数据库配置 ----------------------------
    DB_HOST = os.getenv("DB_HOST")
    DB_PORT = os.getenv("DB_PORT")
    DB_USER = os.getenv("DB_USER")
    DB_PASSWORD = os.getenv("DB_PASSWORD")
    DATABASE = os.getenv("DATABASE")
    DB_URI = 'mysql+pymysql://{username}:{password}@{host}:{port}/{db}?charset-utf8'.format(username=DB_USER,
                                                                                            password=DB_PASSWORD,
                                                                                            host=DB_HOST,
                                                                                            port=DB_PORT,
                                                                                            db=DATABASE)

    # 设置sqlalchemy自动跟踪数据库
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ECHO = False
    # 数据库连接的实例化
    SQLALCHEMY_DATABASE_URI = DB_URI

    # ------------------------------  Scheduler ----------------------------
    JOBS = [
        {  # 任务ID、任务位置、触发器、时间间隔
            'id': 'task1',
            'func': 'app:tasks.scheduler_task.task1',
            'replace_existing': True,
            'trigger': 'interval',
            'seconds': 3600
        },
        {
            'id': 'task2',
            'func': 'app:tasks.scheduler_task.task2',
            'replace_existing': True,
            'args': (1, 2),
            'trigger': 'interval',
            'seconds': 3600
        },
        {
            "id": "task3",
            "func": 'app:tasks.scheduler_task.task3',
            "trigger": "cron",  # cron触发器
            'replace_existing': True,
            "day_of_week": "mon-sun",  # 星期一到星期天每天18点18分0秒执行
            "hour": 18,
            "minute": 18,
            "second": 0
        },
        {
            'id': 'task4_db',
            'func': 'app:tasks.scheduler_task.db_query_todo_all',
            'replace_existing': True,
            'trigger': 'interval',
            'seconds': 3600
        },

    ]
    # 存储位置
    SCHEDULER_JOBSTORES = {
        'default': SQLAlchemyJobStore(url=SQLALCHEMY_DATABASE_URI)
    }
    # 线程池配置
    SCHEDULER_EXECUTORS = {
        'default': {'type': 'threadpool', 'max_workers': 20}
    }
    SCHEDULER_JOB_DEFAULTS = {
        'coalesce': False,
        # 同一个任务同一时间最多有几个实例在跑
        'max_instances': 3,
        'misfire_grace_time': None
    }
    # 配置时区
    SCHEDULER_TIMEZONE = 'Asia/Shanghai'
    # 调度器开关
    SCHEDULER_API_ENABLED = True
    # api前缀（默认是/scheduler）
    SCHEDULER_API_PREFIX = '/scheduler'
    # 设置白名单
    SCHEDULER_ALLOWED_HOSTS = ['*']

    # --------------- 中文乱码问题 -----------------------------------
    # 解决中文乱码的问题，将json数据内的中文正常显示
    JSON_AS_ASCII = False
    # Flask-RESTful 序列化输出中文问题
    # 解决flask接口中文数据编码问题(使用RESTFUL)
    RESTFUL_JSON = dict(ensure_ascii=False)

    # ------------------------------  jwt相关配置 ----------------------------
    # 加密算法,默认: HS256
    JWT_ALGORITHM = "HS256"
    JWT_SECRET_KEY = os.environ.get(
        'JWT_SECRET_KEY') or 'y58Rsqzmts6VCBRHes1Sf2DHdGJaGqPMi6GYpBS4CKyCdi42KLSs9TQVTauZMLMw'
    # token令牌有效期，单位: 秒/s，默认:　datetime.timedelta(minutes=15) 或者 15 * 60
    JWT_ACCESS_TOKEN_EXPIRES = os.environ.get('JWT_ACCESS_TOKEN_EXPIRES') or 15 * 60
    # refresh刷新令牌有效期，单位: 秒/s，默认：datetime.timedelta(days=30) 或者 30*24*60*60
    JWT_REFRESH_TOKEN_EXPIRES = 30 * 24 * 60 * 60

    JWT_COOKIE_CSRF_PROTECT = True
    JWT_CSRF_CHECK_FORM = True
    # 当通过http请求头传递jwt时，请求头参数名称设置，默认值： Authorization
    JWT_HEADER_NAME = "Authorization"
    # 当通过http请求头传递jwt时，令牌的前缀 默认值为 "Bearer"，例如：Authorization: Bearer <JWT>
    JWT_HEADER_TYPE = "Bearer"
    JWT_BLOCKLIST_TOKEN_CHECKS = ['access', 'refresh']
    PROPAGATE_EXCEPTIONS = True

    # -----------------------------  redis相关配置 ----------------------------
    # redis数据库地址
    REDIS_HOST = os.getenv("REDIS_HOST")
    # redis 端口号
    REDIS_PORT = os.getenv("REDIS_PORT")
    # 数据库名
    REDIS_DB = os.getenv("REDIS_DB")
    # redis 过期时间60秒
    REDIS_EXPIRE = os.getenv("REDIS_EXPIRE")


    # 静态方法作为配置的统一接口，暂时为空
    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    """开发模式的配置信息"""

    BEBUG = True
    # 关闭动态跟踪;是否跟踪对象的修改，默认为None，这里设置为False
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class TestingConfig(Config):
    """
    测试环境配置信息
    """
    TESTING = True
    BEBUG = True
    TESTING_DB_Name = 'data-test.sqlite'
    SQLALCHEMY_DATABASE_URI = \
        'sqlite:///' + os.path.join(basedir, TESTING_DB_Name)


class ProductionConfig(Config):
    """生产环境配置信息"""
    DEBUG = False


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig
}
