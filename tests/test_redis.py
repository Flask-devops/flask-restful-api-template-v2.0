import unittest
from utils.redis_util import Redis


class RedisTestCase(unittest.TestCase):
    def test_redis_write(self):
        redis_w = Redis.write("test_key", "test_value", 60)
        self.assertTrue(redis_w)

    def test_redis_read(self):
        redis_r = Redis.read("test_key")
        self.assertEqual(redis_r, "test_value")
