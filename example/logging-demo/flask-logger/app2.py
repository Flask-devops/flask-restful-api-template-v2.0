import logging
from flask import Flask

app = Flask(__name__)
handler = logging.FileHandler(filename="test.log", encoding='utf-8')
handler.setLevel("DEBUG")
format_ = "%(asctime)s[%(name)s][%(levelname)s] :%(levelno)s: %(message)s"
formatter = logging.Formatter(format_)
handler.setFormatter(formatter)
app.logger.addHandler(handler)

app.logger.debug('A value for debugging')
app.logger.warning('A warning occurred (%d apples)', 42)
app.logger.error('An error occurred')
if __name__ == "__main__":
    app.run()
