from flask import Flask, jsonify
from response_msg import ResMsg

app = Flask(__name__)


@app.route("/", methods=["GET"])
def test():
    res = ResMsg()
    test_dict = dict(name="zhang", age=18)
    res.update(data=test_dict)
    return jsonify(res.data)


if __name__ == "__main__":
    app.run()
