"""
安装pip install apscheduler
示例，每5秒输出时间

参考文献：
https://www.qinzhiqiang.xyz/article/9

"""

from apscheduler.schedulers.blocking import BlockingScheduler
from datetime import datetime


def timed_task():
    print(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))


if __name__ == '__main__':
    # 创建当前线程执行的schedulers
    scheduler = BlockingScheduler()
    # 添加调度任务(timed_task),触发器选择interval(间隔性),间隔时长为5秒
    scheduler.add_job(timed_task, 'interval', seconds=5)
    # 启动调度任务
    scheduler.start()
